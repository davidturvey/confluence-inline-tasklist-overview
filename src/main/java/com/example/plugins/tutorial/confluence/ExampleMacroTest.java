package com.example.plugins.tutorial.confluence;
import java.io.*;

import org.junit.Test;

public class ExampleMacroTest {
	
	@Test
    public void basic() throws IOException
    {
		 System.out.println("Working Directory = " +
	              System.getProperty("user.dir"));
		BufferedReader reader = new BufferedReader(new FileReader("./src/main/java/com/example/plugins/tutorial/confluence/TestData"));
		String line = null;
		 StringBuilder inputBuilder = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			inputBuilder.append(line);
		}
		reader.close();
	
		String input = inputBuilder.toString();
		 System.out.println(input);
		 System.out.println();
		
        ExampleMacro obj = new ExampleMacro(null, null);
        String output = obj.ExtractTasksFromPage(input);
        System.out.println(output);
        
    }


}
