package com.example.plugins.tutorial.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

import java.io.StringReader;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class ExampleMacro implements Macro 
{
    private final SpaceManager spaceManager;
    private XhtmlContent xhtmlContent;

    public ExampleMacro(SpaceManager spaceManager, XhtmlContent xhtmlContent) {
        this.spaceManager = spaceManager;
        this.xhtmlContent = xhtmlContent;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    @Override
    public String execute(Map<String, String> parameters, String bodyContent,
        ConversionContext conversionContext) throws MacroExecutionException 
    {
        String spaceKey = conversionContext.getSpaceKey().toUpperCase();
        Space currSpace = spaceManager.getSpace(spaceKey);
        Page pages = currSpace.getHomePage();

        // Pre-format table
        StringBuilder builder = new StringBuilder();
        builder.append("<p>").append("<div class=\"table-wrap\">")
        .append("<table width=\"100%\" class=\"confluenceTable\">")
        .append("<tbody>");
        builder.append("<tr><th>Task</th></tr>");

        // Search all pages for incomplete tasks in meeting minutes
        for (Page page : pages.getDescendents()) 
        {
            String pageBody = page.getBodyAsString();

            // ignore pages without task-list and without incomplete tasks 
            if (!pageBody.contains("<ac:task-list>") || !pageBody.contains("incomplete"))
                continue; 

            // Add link to protocol
            builder.append("<tr><th><ac:link><ri:page ri:content-title=");
            builder.append("\"" + page.getTitle() + "\"");
            builder.append("/><ac:link-body>");
            builder.append("Page: " + page.getTitle()); 
            builder.append("</ac:link-body></ac:link></th></tr>");
         //   builder.append(pageBody);
           
             
            builder.append(ExtractTasksFromPage(pageBody));
            
           
        }
        // Close table
        builder.append("</tbody>");
        builder.append("</table>");
        builder.append("</div>");
        builder.append("</p>");

        // Return converted string
        try 
        {
                return xhtmlContent.convertStorageToView(builder.toString(), conversionContext);
        } 
        catch (XMLStreamException e) 
        {
                return e.toString();
        } 
        catch (XhtmlException e) 
        {
           return e.toString();
        }
    }
    
    public String ExtractTasksFromPage(String pageBody)
    {
    	 StringBuilder builder = new StringBuilder();
        // ignore pages without task-list and without incomplete tasks 
        if (!pageBody.contains("<ac:task-list>") || !pageBody.contains("incomplete"))
            return "";     
                
        String taskTag = "ac:task";
        String taskOpenTag = "<"+taskTag+">";
        String taskCloseTag = "</"+taskTag+">";
        
        String taskStatusTag = "ac:task-status";
        String taskStatusOpenTag = "<"+taskStatusTag+">";
        String taskStatusCloseTag = "</"+taskStatusTag+">";
        
        String taskBodyTag = "ac:task-body";
        String taskBodyOpenTag = "<"+taskBodyTag+">";
        String taskBodyCloseTag = "</"+taskBodyTag+">";
        
        int trStartIndex = pageBody.indexOf(taskOpenTag);
        int trEndIndex = pageBody.indexOf(taskCloseTag, trStartIndex);
        
        while (trStartIndex >= 0) 
        {
        	String taskStr = pageBody.substring(trStartIndex + taskOpenTag.length(), trEndIndex );
        	
        	String status = taskStr.substring(taskStr.indexOf(taskStatusOpenTag) + taskStatusOpenTag.length(), taskStr.indexOf(taskStatusCloseTag) );
        	String body = taskStr.substring(taskStr.indexOf(taskBodyOpenTag) + taskBodyOpenTag.length(), taskStr.indexOf(taskBodyCloseTag) );
        	
        	if(status.equals("incomplete"))
        	{
        		builder.append("<tr><td>");
        		builder.append(body);                        
        		builder.append("</td></tr>");     
        	}
        	
      
            // find next line
            trStartIndex = pageBody.indexOf(taskOpenTag, trEndIndex);
            trEndIndex = pageBody.indexOf(taskCloseTag, trStartIndex);
            
       
    }
        return builder.toString();
    }
}