Macro-Plugin for confluence.
Generates an overview of all incomplete inline tasks in the current space.

Layout must be a 2 column table where the 2nd column holds the tasks.